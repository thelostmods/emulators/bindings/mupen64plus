## Mupen64Plus-D

This is a package for interacting with mupen64plus set by the m64p_core api standard.

All interaction operates like its natural C counterpart, and is bound using dynalib to runtime-load conveniently.

Referenced source:
https://github.com/mupen64plus/mupen64plus-core/tree/master/src/api