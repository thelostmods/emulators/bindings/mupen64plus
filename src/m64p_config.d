module m64p_config;

import m64p_frontend;
import m64p_types;

static extern(C):

    /* ConfigListSections()
    *
    * This function is called to enumerate the list of Sections in the Mupen64Plus
    * configuration file. It is expected that there will be a section named "Core"
    * for core-specific configuration data, "Graphics" for common graphics options,
    * and one or more sections for each plugin library. 
    */
    m64p_error function(void*, void function(void*, const char*)) ConfigListSections;

    /* ConfigOpenSection()
    *
    * This function is used to give a configuration section handle to the front-end
    * which may be used to read or write configuration parameter values in a given
    * section of the configuration file.
    */
    m64p_error function(const char*, m64p_handle*) ConfigOpenSection;

    /* ConfigListParameters()
    *
    * This function is called to enumerate the list of Parameters in a given
    * Section of the Mupen64Plus configuration file. 
    */
    m64p_error function(m64p_handle, void*, void function(void*, const char*, m64p_type)) ConfigListParameters;

    /* ConfigSaveFile()
    *
    * This function saves the entire current Mupen64Plus configuration to disk.
    */
    m64p_error function() ConfigSaveFile;

    /* ConfigSaveSection()
    *
    * This function saves one section of the current Mupen64Plus configuration to disk.
    */
    m64p_error function(const char*) ConfigSaveSection;

    /* ConfigHasUnsavedChanges()
    *
    * This function determines if a given Section (or all sections) of the Mupen64Plus Core configuration file has been modified since it was last saved or loaded.
    */
    int function(const char*) ConfigHasUnsavedChanges;

    /* ConfigDeleteSection()
    *
    * This function deletes a section from the Mupen64Plus configuration data.
    */
    m64p_error function(const char* SectionName) ConfigDeleteSection;

    /* ConfigRevertChanges()
    *
    * This function reverts changes previously made to one section of the configuration file, so that it will match with the configuration at the last time that it was loaded from or saved to disk.
    */
    m64p_error function(const char *SectionName) ConfigRevertChanges;
    
    /* ConfigSetParameter()
    *
    * This function sets the value of one of the emulator's configuration
    * parameters.
    */
    m64p_error function(m64p_handle, const char*, m64p_type, const void*) ConfigSetParameter;

    /* ConfigSetParameterHelp()
    *
    * This function sets the help string of one of the emulator's configuration
    * parameters.
    */
    m64p_error function(m64p_handle, const char*, const char*) ConfigSetParameterHelp;
    
    /* ConfigGetParameter()
    *
    * This function retrieves the value of one of the emulator's parameters. 
    */
    m64p_error function(m64p_handle, const char*, m64p_type, void*, int) ConfigGetParameter;

    /* ConfigGetParameterType()
    *
    * This function retrieves the type of one of the emulator's parameters. 
    */
    m64p_error function(m64p_handle, const char*, m64p_type*) ConfigGetParameterType;

    /* ConfigGetParameterHelp()
    *
    * This function retrieves the help information about one of the emulator's
    * parameters.
    */
    const(char)* function(m64p_handle, const char*) ConfigGetParameterHelp;

    /* ConfigSetDefault***()
    *
    * These functions are used to set the value of a configuration parameter if it
    * is not already present in the configuration file. This may happen if a new
    * user runs the emulator, or an upgraded module uses a new parameter, or the
    * user deletes his or her configuration file. If the parameter is already
    * present in the given section of the configuration file, then no action will
    * be taken and this function will return successfully.
    */
    m64p_error function(m64p_handle, const char*, int, const char*) ConfigSetDefaultInt;
    m64p_error function(m64p_handle, const char*, float, const char*) ConfigSetDefaultFloat;
    m64p_error function(m64p_handle, const char*, int, const char*) ConfigSetDefaultBool;
    m64p_error function(m64p_handle, const char*, const char*, const char*) ConfigSetDefaultString;

    /* ConfigGetParam***()
    *
    * These functions retrieve the value of one of the emulator's parameters in
    * the given section, and return the value directly to the calling function. If
    * an errors occurs (such as an invalid Section handle, or invalid
    * configuration parameter name), then an error will be sent to the front-end
    * via the DebugCallback() function, and either a 0 (zero) or an empty string
    * will be returned.
    */
    int function(m64p_handle, const char*) ConfigGetParamInt;
    float function(m64p_handle, const char*) ConfigGetParamFloat;
    int function(m64p_handle, const char*) ConfigGetParamBool;
    const(char)* function(m64p_handle, const char*) ConfigGetParamString;

    /* ConfigGetSharedDataFilepath()
    *
    * This function is provided to allow a plugin to retrieve a full pathname to a
    * given shared data file. This type of file is intended to be shared among
    * multiple users on a system, so it is likely to be read-only.
    */
    const(char)* function(const char*) ConfigGetSharedDataFilepath;

    /* ConfigGetUserConfigPath()
    *
    * This function may be used by the plugins or front-end to get a path to the
    * directory for storing user-specific configuration files. This will be the
    * directory where "mupen64plus.cfg" is located.
    */
    const(char)* function() ConfigGetUserConfigPath;

    /* ConfigGetUserDataPath()
    *
    * This function may be used by the plugins or front-end to get a path to the
    * directory for storing user-specific data files. This may be used to store
    * files such as screenshots, saved game states, or hi-res textures.
    */
    const(char)* function() ConfigGetUserDataPath;

    /* ConfigGetUserCachePath()
    *
    * This function may be used by the plugins or front-end to get a path to the
    * directory for storing cached user-specific data files. Files in this
    * directory may be deleted by the user to save space, so critical information
    * should not be stored here.  This directory may be used to store files such
    * as the ROM browser cache.
    */
    const(char)* function() ConfigGetUserCachePath;

    /* ConfigExternalOpen()
    *
    * This function reads the contents of the config file into memory
    * and returns M64ERR_SUCCESS if successful.
    */
    m64p_error function(const char*, m64p_handle*) ConfigExternalOpen;

    /* ConfigExternalClose()
    *
    * Frees the memory pointer created by ConfigExternalOpen.
    */
    m64p_error function(m64p_handle) ConfigExternalClose;

    /* ConfigExternalGetParameter()
    *
    * This functions allows a plugin to leverage the built-in ini parser to read
    * any cfg/ini file. It will return M64ERR_SUCCESS if the item was found.
    */
    m64p_error function(m64p_handle, const char*, const char*, char*, int) ConfigExternalGetParameter;

    /* ConfigSendNetplayConfig()
    *
    * This function allows plugins to take advantage of the netplay TCP connection
    * to send configuration data to the netplay server.
    */
    m64p_error function(char*, int) ConfigSendNetplayConfig;

    /* ConfigReceiveNetplayConfig()
    *
    * This function allows plugins to take advantage of the netplay TCP connection
    * to receive configuration data from the netplay server.
    */
    m64p_error function(char*, int) ConfigReceiveNetplayConfig;

    /* ConfigOverrideUserPaths()
    *
    * This function allows overriding the paths returned by
    * ConfigGetUserDataPath and ConfigGetUserCachePath
    */
    m64p_error function(const char*, const char*) ConfigOverrideUserPaths;
