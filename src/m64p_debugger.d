module m64p_debugger;

import m64p_types;

static extern(C):

    /* DebugSetCallbacks()
    *
    * This function is called by the front-end to supply debugger callback
    * function pointers. If debugger is enabled and then later disabled within the
    * UI, this function may be called with NULL pointers in order to disable the
    * callbacks. 
    */
    m64p_error function(void function(), void function(uint), void function()) DebugSetCallbacks;

    /* DebugSetCoreCompare()
    *
    * This function is called by the front-end to supply callback function pointers
    * for the Core Comparison feature.
    */
    m64p_error function(void function(uint), void function(int, void*)) DebugSetCoreCompare;

    /* DebugSetRunState()
    *
    * This function sets the run state of the R4300 CPU emulator.
    */
    m64p_error function(m64p_dbg_runstate) DebugSetRunState;

    /* DebugGetState()
    *
    * This function reads and returns a debugger state variable, which are
    * enumerated in m64p_types.h. 
    */
    int function(m64p_dbg_state) DebugGetState;

    /* DebugStep()
    *
    * This function signals the debugger to advance one instruction when in the
    * stepping mode.
    */
    m64p_error function() DebugStep;

    /* DebugDecodeOp()
    *
    * This is a helper function for the debugger front-end. This instruction takes
    * a PC value and an R4300 instruction opcode and writes the disassembled
    * instruction mnemonic and arguments into character buffers. This is intended
    * to be used to display disassembled code. 
    */
    void function(uint, char*, char*, int) DebugDecodeOp;

    /* DebugMemGetRecompInfo()
    *
    * This function is used by the front-end to retrieve disassembly information
    * about recompiled code. For example, the dynamic recompiler may take a single
    * R4300 instruction and compile it into 10 x86 instructions. This function may
    * then be used to retrieve the disassembled code of the 10 x86 instructions.
    */
    void* function(m64p_dbg_mem_info, uint, int) DebugMemGetRecompInfo;

    /* DebugMemGetMemInfo()
    *
    * This function returns an integer value regarding the memory location address,
    * corresponding to the information requested by mem_info_type, which is a type
    * enumerated in m64p_types.h.
    */
    int function(m64p_dbg_mem_info, uint) DebugMemGetMemInfo;

    /* DebugMemGetPointer()
    *
    * This function returns a memory pointer (in x86 memory space) to a block of
    * emulated N64 memory. This may be used to retrieve a pointer to a special N64
    * block (such as the serial, video, or audio registers) or the RDRAM.
    */
    void* function(m64p_dbg_mem_type) DebugMemGetPointer;

    /* DebugMemRead**()
    *
    * These functions retrieve a value from the emulated N64 memory. The returned
    * value will be correctly byte-swapped for the host architecture. 
    */
    ulong function(uint) DebugMemRead64;
    uint function(uint) DebugMemRead32;
    ushort function(uint) DebugMemRead16;
    ubyte function(uint) DebugMemRead8;

    /* DebugMemWrite**()
    *
    * These functions write a value into the emulated N64 memory. The given value
    * will be correctly byte-swapped before storage. 
    */
    void function(uint, ulong) DebugMemWrite64;
    void function(uint, uint) DebugMemWrite32;
    void function(uint, ushort) DebugMemWrite16;
    void function(uint, ubyte) DebugMemWrite8;

    /* DebugGetCPUDataPtr()
    *
    * This function returns a memory pointer (in x86 memory space) to a specific
    * register in the emulated R4300 CPU.
    */
    void* function(m64p_dbg_cpu_data) DebugGetCPUDataPtr;

    /* DebugBreakpointLookup()
    *
    * This function searches through all current breakpoints in the debugger to
    * find one that matches the given input parameters. If a matching breakpoint
    * is found, the index number is returned. If no breakpoints are found, -1 is
    * returned. 
    */
    int function(uint, uint, uint) DebugBreakpointLookup;

    /* DebugBreakpointCommand()
    *
    * This function is used to process common breakpoint commands, such as adding,
    * removing, or searching the breakpoints. The meanings of the index and ptr
    * input parameters vary by command.
    */
    int function(m64p_dbg_bkp_command, uint, m64p_breakpoint*) DebugBreakpointCommand;

    /* DebugBreakpointTriggeredBy()
    *
    * This function is used to retrieve the trigger flags and address for the
    * most recently triggered breakpoint.
    */
    void function(uint*, uint*) DebugBreakpointTriggeredBy;

    /* DebugVirtualToPhysical()
    *
    * This function is used to translate virtual addresses to physical addresses.
    * Memory read/write breakpoints operate on physical addresses.
    */
    uint function(uint) DebugVirtualToPhysical;
