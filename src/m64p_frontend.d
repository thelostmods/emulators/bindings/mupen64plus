module m64p_frontend;

import m64p_types;

/* pointer types to the callback functions in the front-end application */
alias DebugCallback = void function(void* Context, int level, const char*  message);
alias StateCallback = void function(void* Context, m64p_core_param param_type, int new_value);

static extern(C):
    /* CoreStartup()
    *
    * This function initializes libmupen64plus for use by allocating memory,
    * creating data structures, and loading the configuration file.
    */
    m64p_error function(int, const char*, const char*, void*, DebugCallback, void*, StateCallback) CoreStartup;

    /* CoreShutdown()
    *
    * This function saves the configuration file, then destroys data structures
    * and releases memory allocated by the core library.
    */
    m64p_error function() CoreShutdown;

    /* CoreAttachPlugin()
    *
    * This function attaches the given plugin to the emulator core. There can only
    * be one plugin of each type attached to the core at any given time. 
    */
    m64p_error function(m64p_plugin_type, m64p_dynlib_handle) CoreAttachPlugin;

    /* CoreDetachPlugin()
    *
    * This function detaches the given plugin from the emulator core, and re-attaches
    * the 'dummy' plugin functions. 
    */
    m64p_error function(m64p_plugin_type) CoreDetachPlugin;

    /* CoreDoCommand()
    *
    * This function sends a command to the emulator core.
    */
    m64p_error function(m64p_command, int, void*) CoreDoCommand;

    /* CoreOverrideVidExt()
    *
    * This function overrides the core's internal SDL-based OpenGL functions. This
    * override functionality allows a front-end to define its own video extension
    * functions to be used instead of the SDL functions. If any of the function
    * pointers in the structure are NULL, the override function will be disabled
    * and the core's internal SDL functions will be used.
    */
    m64p_error function(m64p_video_extension_functions*) CoreOverrideVidExt;

    /* CoreAddCheat()
    *
    * This function will add a Cheat Function to a list of currently active cheats
    * which are applied to the open ROM.
    */
    m64p_error function(const char*, m64p_cheat_code*, int) CoreAddCheat;

    /* CoreCheatEnabled()
    *
    * This function will enable or disable a Cheat Function which is in the list of
    * currently active cheats.
    */
    m64p_error function(m64p_rom_settings*, int, int, int) CoreCheatEnabled;

    /* CoreGetRomSettings()
    *
    * This function will retrieve the ROM settings from the mupen64plus INI file for
    * the ROM image corresponding to the given CRC values.
    */
    m64p_error function(m64p_rom_settings*, int, int, int) CoreGetRomSettings;

    /* CoreSaveOverride()
    *
    * This function will override where the eep save files are targetting.
    */
    m64p_error function(const char* path) CoreSaveOverride;

    /***********************\
    |*  MODDING FUNCTIONS  *|
    \***********************/

    /* GetHeader()
    *
    * This function will return a pointer to the ROM_HEADER.
    */
    void* function() GetHeader;

    /* GetRdRam()
    *
    * This function will return a pointer to the RDRAM.
    */
    void* function() GetRdRam;

    /* GetRom()
    *
    * This function will return a pointer to the ROM.
    */
    void* function() GetRom;

    /* GetRdRamSize()
    *
    * This function will return the real length of RDRAM memory.
    */
    const(size_t) function() GetRdRamSize;

    /* GetRomSize()
    *
    * This function will return the real length of ROM memory.
    */
    const(size_t) function() GetRomSize;

    /* RefreshDynarec()
    *
    * This function will refresh the dynamic recompiler needed for asm injections to work.
    */
    void function() RefreshDynarec;