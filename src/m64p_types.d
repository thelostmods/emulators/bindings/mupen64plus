module m64p_types;

import dynalib : expandEnum;

alias m64p_dynlib_handle = void*;
alias m64p_handle = void*;

/* ----------------------------------------- */
/* Structures and Types for Core library API */
/* ----------------------------------------- */

alias m64p_function       = void function();
alias m64p_frame_callback = void function(uint FrameIndex);
alias m64p_input_callback = void function();
alias m64p_audio_callback = void function();
alias m64p_vi_callback    = void function();

enum m64p_type
{
  M64TYPE_INT = 1,
  M64TYPE_FLOAT,
  M64TYPE_BOOL,
  M64TYPE_STRING
} mixin(expandEnum!m64p_type);

enum m64p_msg_level
{
  M64MSG_ERROR = 1,
  M64MSG_WARNING,
  M64MSG_INFO,
  M64MSG_STATUS,
  M64MSG_VERBOSE
} mixin(expandEnum!m64p_msg_level);

enum m64p_error
{
  M64ERR_SUCCESS = 0,
  M64ERR_NOT_INIT,        /* Function is disallowed before InitMupen64Plus() is called */
  M64ERR_ALREADY_INIT,    /* InitMupen64Plus() was called twice */
  M64ERR_INCOMPATIBLE,    /* API versions between components are incompatible */
  M64ERR_INPUT_ASSERT,    /* Invalid parameters for function call, such as ParamValue=NULL for GetCoreParameter() */
  M64ERR_INPUT_INVALID,   /* Invalid input data, such as ParamValue="maybe" for SetCoreParameter() to set a BOOL-type value */
  M64ERR_INPUT_NOT_FOUND, /* The input parameter(s) specified a particular item which was not found */
  M64ERR_NO_MEMORY,       /* Memory allocation failed */
  M64ERR_FILES,           /* Error opening, creating, reading, or writing to a file */
  M64ERR_INTERNAL,        /* Internal error (bug) */
  M64ERR_INVALID_STATE,   /* Current program state does not allow operation */
  M64ERR_PLUGIN_FAIL,     /* A plugin function returned a fatal error */
  M64ERR_SYSTEM_FAIL,     /* A system function call, such as an SDL or file operation, failed */
  M64ERR_UNSUPPORTED,     /* Function call is not supported (ie, core not built with debugger) */
  M64ERR_WRONG_TYPE       /* A given input type parameter cannot be used for desired operation */
} mixin(expandEnum!m64p_error);

enum m64p_core_caps
{
  M64CAPS_DYNAREC = 1,
  M64CAPS_DEBUGGER = 2,
  M64CAPS_CORE_COMPARE = 4
} mixin(expandEnum!m64p_core_caps);

enum m64p_plugin_type
{
  M64PLUGIN_NULL = 0,
  M64PLUGIN_RSP = 1,
  M64PLUGIN_GFX,
  M64PLUGIN_AUDIO,
  M64PLUGIN_INPUT,
  M64PLUGIN_CORE
} mixin(expandEnum!m64p_plugin_type);

enum m64p_emu_state
{
  M64EMU_STOPPED = 1,
  M64EMU_RUNNING,
  M64EMU_PAUSED
} mixin(expandEnum!m64p_emu_state);

enum m64p_video_mode
{
  M64VIDEO_NONE = 1,
  M64VIDEO_WINDOWED,
  M64VIDEO_FULLSCREEN
} mixin(expandEnum!m64p_video_mode);

enum m64p_video_flags
{
  M64VIDEOFLAG_SUPPORT_RESIZING = 1
} mixin(expandEnum!m64p_video_flags);

enum m64p_core_param
{
  M64CORE_EMU_STATE = 1,
  M64CORE_VIDEO_MODE,
  M64CORE_SAVESTATE_SLOT,
  M64CORE_SPEED_FACTOR,
  M64CORE_SPEED_LIMITER,
  M64CORE_VIDEO_SIZE,
  M64CORE_AUDIO_VOLUME,
  M64CORE_AUDIO_MUTE,
  M64CORE_INPUT_GAMESHARK,
  M64CORE_STATE_LOADCOMPLETE,
  M64CORE_STATE_SAVECOMPLETE
} mixin(expandEnum!m64p_core_param);

enum m64p_command
{
  M64CMD_NOP = 0,
  M64CMD_ROM_OPEN,
  M64CMD_ROM_CLOSE,
  M64CMD_ROM_GET_HEADER,
  M64CMD_ROM_GET_SETTINGS,
  M64CMD_EXECUTE,
  M64CMD_STOP,
  M64CMD_PAUSE,
  M64CMD_RESUME,
  M64CMD_CORE_STATE_QUERY,
  M64CMD_STATE_LOAD,
  M64CMD_STATE_SAVE,
  M64CMD_STATE_SET_SLOT,
  M64CMD_SEND_SDL_KEYDOWN,
  M64CMD_SEND_SDL_KEYUP,
  M64CMD_SET_FRAME_CALLBACK,
  M64CMD_TAKE_NEXT_SCREENSHOT,
  M64CMD_CORE_STATE_SET,
  M64CMD_READ_SCREEN,
  M64CMD_RESET,
  M64CMD_ADVANCE_FRAME,
  M64CMD_SET_MEDIA_LOADER,
  M64CMD_NETPLAY_INIT,
  M64CMD_NETPLAY_CONTROL_PLAYER,
  M64CMD_NETPLAY_GET_VERSION,
  M64CMD_NETPLAY_CLOSE,
  M64CMD_PIF_OPEN,
  M64CMD_ROM_SET_SETTINGS
} mixin(expandEnum!m64p_command);

struct m64p_cheat_code
{
  uint address;
  int  value;
}

struct m64p_media_loader
{
  static extern(C):
    /* Frontend-defined callback data. */
    void* cb_data;

    /* Allow the frontend to specify the GB cart ROM file to load
    * cb_data: points to frontend-defined callback data.
    * controller_num: (0-3) tell the frontend which controller is about to load a GB cart
    * Returns a NULL-terminated string owned by the core specifying the GB cart ROM filename to load.
    * Empty or NULL string results in no GB cart being loaded (eg. empty transferpak).
    */
    char* function(void* cb_data, int controller_num) get_gb_cart_rom;

    /* Allow the frontend to specify the GB cart RAM file to load
    * cb_data: points to frontend-defined callback data.
    * controller_num: (0-3) tell the frontend which controller is about to load a GB cart
    * Returns a NULL-terminated string owned by the core specifying the GB cart RAM filename to load
    * Empty or NULL string results in the core generating a default save file with empty content.
    */
    char* function(void* cb_data, int controller_num) get_gb_cart_ram;

    /* Allow the frontend to specify the DD IPL ROM file to load
    * cb_data: points to frontend-defined callback data.
    * Returns a NULL-terminated string owned by the core specifying the DD IPL ROM filename to load
    * Empty or NULL string results in disabled 64DD.
    */
    char* function(void* cb_data) get_dd_rom;

    /* Allow the frontend to specify the DD disk file to load
    * cb_data: points to frontend-defined callback data.
    * Returns a NULL-terminated string owned by the core specifying the DD disk filename to load
    * Empty or NULL string results in no DD disk being loaded (eg. empty disk drive).
    */
    char* function(void* cb_data) get_dd_disk;
}

/* ----------------------------------------- */
/* Structures to hold ROM image information  */
/* ----------------------------------------- */

enum m64p_system_type
{
    SYSTEM_NTSC = 0,
    SYSTEM_PAL,
    SYSTEM_MPAL
} mixin(expandEnum!m64p_system_type);

enum m64p_rom_save_type
{
    SAVETYPE_EEPROM_4KB = 0,
    SAVETYPE_EEPROM_16KB,
    SAVETYPE_SRAM,
    SAVETYPE_FLASH_RAM,
    SAVETYPE_CONTROLLER_PACK,
    SAVETYPE_NONE
} mixin(expandEnum!m64p_rom_save_type);

struct m64p_rom_header
{
   ubyte     init_PI_BSB_DOM1_LAT_REG;  /* 0x00 */
   ubyte     init_PI_BSB_DOM1_PGS_REG;  /* 0x01 */
   ubyte     init_PI_BSB_DOM1_PWD_REG;  /* 0x02 */
   ubyte     init_PI_BSB_DOM1_PGS_REG2; /* 0x03 */
   uint      ClockRate;                 /* 0x04 */
   uint      PC;                        /* 0x08 */
   uint      Release;                   /* 0x0C */
   uint      CRC1;                      /* 0x10 */
   uint      CRC2;                      /* 0x14 */
   uint[2]   Unknown;                   /* 0x18 */
   ubyte[20] Name;                      /* 0x20 */
   uint      unknown;                   /* 0x34 */
   uint      Manufacturer_ID;           /* 0x38 */
   ushort    Cartridge_ID;              /* 0x3C - Game serial number  */
   ushort    Country_code;              /* 0x3E */
}

struct m64p_rom_settings
{
   char[256] goodname;
   char[33] MD5;
   ubyte savetype;
   ubyte status;          /* Rom status on a scale from 0-5. */
   ubyte players;         /* Local players 0-4, 2/3/4 way Netplay indicated by 5/6/7. */
   ubyte rumble;          /* 0 - No, 1 - Yes boolean for rumble support. */
   ubyte transferpak;     /* 0 - No, 1 - Yes boolean for transfer pak support. */
   ubyte mempak;          /* 0 - No, 1 - Yes boolean for memory pak support. */
   ubyte biopak;          /* 0 - No, 1 - Yes boolean for bio pak support. */
   ubyte disableextramem; /* 0 - No, 1 - Yes boolean for disabling 4MB expansion RAM pack */
   uint countperop;       /* Number of CPU cycles per instruction. */
   uint sidmaduration;    /* Default SI DMA duration */
}

/* ----------------------------------------- */
/* Structures and Types for the Debugger     */
/* ----------------------------------------- */

enum m64p_dbg_state
{
  M64P_DBG_RUN_STATE = 1,
  M64P_DBG_PREVIOUS_PC,
  M64P_DBG_NUM_BREAKPOINTS,
  M64P_DBG_CPU_DYNACORE,
  M64P_DBG_CPU_NEXT_INTERRUPT
} mixin(expandEnum!m64p_dbg_state);

enum m64p_dbg_runstate
{
  M64P_DBG_RUNSTATE_PAUSED = 0,
  M64P_DBG_RUNSTATE_STEPPING,
  M64P_DBG_RUNSTATE_RUNNING
} mixin(expandEnum!m64p_dbg_runstate);

enum m64p_dbg_mem_info
{
  M64P_DBG_MEM_TYPE = 1,
  M64P_DBG_MEM_FLAGS,
  M64P_DBG_MEM_HAS_RECOMPILED,
  M64P_DBG_MEM_NUM_RECOMPILED,
  M64P_DBG_RECOMP_OPCODE = 16,
  M64P_DBG_RECOMP_ARGS,
  M64P_DBG_RECOMP_ADDR
} mixin(expandEnum!m64p_dbg_mem_info);

enum m64p_dbg_mem_type
{
  M64P_MEM_NOMEM = 0,
  M64P_MEM_NOTHING,
  M64P_MEM_RDRAM,
  M64P_MEM_RDRAMREG,
  M64P_MEM_RSPMEM,
  M64P_MEM_RSPREG,
  M64P_MEM_RSP,
  M64P_MEM_DP,
  M64P_MEM_DPS,
  M64P_MEM_VI,
  M64P_MEM_AI,
  M64P_MEM_PI,
  M64P_MEM_RI,
  M64P_MEM_SI,
  M64P_MEM_FLASHRAMSTAT,
  M64P_MEM_ROM,
  M64P_MEM_PIF,
  M64P_MEM_MI,
  M64P_MEM_BREAKPOINT
} mixin(expandEnum!m64p_dbg_mem_type);

enum m64p_dbg_mem_flags
{
  M64P_MEM_FLAG_READABLE = 0x01,
  M64P_MEM_FLAG_WRITABLE = 0x02,
  M64P_MEM_FLAG_READABLE_EMUONLY = 0x04,  /* the EMUONLY flags signify that emulated code can read/write here, but debugger cannot */
  M64P_MEM_FLAG_WRITABLE_EMUONLY = 0x08
} mixin(expandEnum!m64p_dbg_mem_flags);

enum m64p_dbg_memptr_type
{
  M64P_DBG_PTR_RDRAM = 1,
  M64P_DBG_PTR_PI_REG,
  M64P_DBG_PTR_SI_REG,
  M64P_DBG_PTR_VI_REG,
  M64P_DBG_PTR_RI_REG,
  M64P_DBG_PTR_AI_REG
} mixin(expandEnum!m64p_dbg_memptr_type);

enum m64p_dbg_cpu_data
{
  M64P_CPU_PC = 1,
  M64P_CPU_REG_REG,
  M64P_CPU_REG_HI,
  M64P_CPU_REG_LO,
  M64P_CPU_REG_COP0,
  M64P_CPU_REG_COP1_DOUBLE_PTR,
  M64P_CPU_REG_COP1_SIMPLE_PTR,
  M64P_CPU_REG_COP1_FGR_64,
  M64P_CPU_TLB
} mixin(expandEnum!m64p_dbg_cpu_data);

enum m64p_dbg_bkp_command
{
  M64P_BKP_CMD_ADD_ADDR = 1,
  M64P_BKP_CMD_ADD_STRUCT,
  M64P_BKP_CMD_REPLACE,
  M64P_BKP_CMD_REMOVE_ADDR,
  M64P_BKP_CMD_REMOVE_IDX,
  M64P_BKP_CMD_ENABLE,
  M64P_BKP_CMD_DISABLE,
  M64P_BKP_CMD_CHECK
} mixin(expandEnum!m64p_dbg_bkp_command);

enum M64P_MEM_INVALID       = 0xFFFFFFFF;  /* invalid memory read will return this */

enum BREAKPOINTS_MAX_NUMBER = 128;

enum m64p_dbg_bkp_flags
{
  M64P_BKP_FLAG_ENABLED = 0x01,
  M64P_BKP_FLAG_READ = 0x02,
  M64P_BKP_FLAG_WRITE = 0x04,
  M64P_BKP_FLAG_EXEC = 0x08,
  M64P_BKP_FLAG_LOG = 0x10 /* Log to the console when this breakpoint hits */
} mixin(expandEnum!m64p_dbg_bkp_flags);

import std.traits : isNumeric;
bool BPT_CHECK_FLAG(T)(ref m64p_breakpoint a, T b) if (isNumeric!T) { return (a.flags & b) == b; }
void BPT_SET_FLAG(T)(ref m64p_breakpoint a, T b) if (isNumeric!T) { a.flags = (a.flags | b); }
void BPT_CLEAR_FLAG(T)(ref m64p_breakpoint a, T b) if (isNumeric!T) { a.flags = (a.flags & (~b)); }
void BPT_TOGGLE_FLAG(T)(ref m64p_breakpoint a, T b) if (isNumeric!T) { a.flags = (a.flags ^ b); }

struct m64p_breakpoint
{
  uint address;
  uint endaddr;
  uint flags;
}

/* ------------------------------------------------- */
/* Structures and Types for Core Video Extension API */
/* ------------------------------------------------- */

struct m64p_2d_size
{
  uint uiWidth;
  uint uiHeight;
}

enum m64p_GLattr
{
  M64P_GL_DOUBLEBUFFER = 1,
  M64P_GL_BUFFER_SIZE,
  M64P_GL_DEPTH_SIZE,
  M64P_GL_RED_SIZE,
  M64P_GL_GREEN_SIZE,
  M64P_GL_BLUE_SIZE,
  M64P_GL_ALPHA_SIZE,
  M64P_GL_SWAP_CONTROL,
  M64P_GL_MULTISAMPLEBUFFERS,
  M64P_GL_MULTISAMPLESAMPLES,
  M64P_GL_CONTEXT_MAJOR_VERSION,
  M64P_GL_CONTEXT_MINOR_VERSION,
  M64P_GL_CONTEXT_PROFILE_MASK
} mixin(expandEnum!m64p_GLattr);

enum m64p_GLContextType
{
  M64P_GL_CONTEXT_PROFILE_CORE,
  M64P_GL_CONTEXT_PROFILE_COMPATIBILITY,
  M64P_GL_CONTEXT_PROFILE_ES
} mixin(expandEnum!m64p_GLContextType);

struct m64p_video_extension_functions
{
  static extern(C):
    uint          Functions;
    m64p_error    function() VidExtFuncInit;
    m64p_error    function() VidExtFuncQuit;
    m64p_error    function(m64p_2d_size*, int*) VidExtFuncListModes;
    m64p_error    function(m64p_2d_size, int*, int*) VidExtFuncListRates;
    m64p_error    function(int, int, int, int, int) VidExtFuncSetMode;
    m64p_error    function(int, int, int, int, int, int) VidExtFuncSetModeWithRate;
    m64p_function function(const char*) VidExtFuncGLGetProc;
    m64p_error    function(m64p_GLattr, int) VidExtFuncGLSetAttr;
    m64p_error    function(m64p_GLattr, int*) VidExtFuncGLGetAttr;
    m64p_error    function() VidExtFuncGLSwapBuf;
    m64p_error    function(const char*) VidExtFuncSetCaption;
    m64p_error    function() VidExtFuncToggleFS;
    m64p_error    function(int, int) VidExtFuncResizeWindow;
    uint          function() VidExtFuncGLGetDefaultFramebuffer;
}