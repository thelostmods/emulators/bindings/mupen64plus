module m64p_vidext;

import m64p_types;

static extern(C):

    /* VidExt_Init()
    *
    * This function should be called from within the InitiateGFX() video plugin
    * function call. The default SDL implementation of this function simply calls
    * SDL_InitSubSystem(SDL_INIT_VIDEO). It does not open a rendering window or
    * switch video modes.
    */
    m64p_error function() VidExt_Init;

    /* VidExt_Quit()
    *
    * This function closes any open rendering window and shuts down the video
    * system. The default SDL implementation of this function calls
    * SDL_QuitSubSystem(SDL_INIT_VIDEO). This function should be called from
    * within the RomClose() video plugin function.
    */
    m64p_error function() VidExt_Quit;

    /* VidExt_ListFullscreenModes()
    *
    * This function is used to enumerate the available resolutions for fullscreen
    * video modes. A pointer to an array is passed into the function, which is
    * then filled with resolution sizes.
    */
    m64p_error function(m64p_2d_size*, int*) VidExt_ListFullscreenModes;

    /* VidExt_ListFullscreenRates()
    *
    * This function is used to enumerate the available refresh rates for a fullscreen
    * video mode.
    */
    m64p_error function(m64p_2d_size, int*, int*) VidExt_ListFullscreenRates;

    /* VidExt_SetVideoMode()
    *
    * This function creates a rendering window or switches into a fullscreen
    * video mode. Any desired OpenGL attributes should be set before calling
    * this function.
    */
    m64p_error function(int, int, int, m64p_video_mode, m64p_video_flags) VidExt_SetVideoMode;

    /* VidExt_SetVideoModeWithRate()
    *
    * This function creates a rendering window or switches into a fullscreen
    * video mode. Any desired OpenGL attributes should be set before calling
    * this function.
    */
    m64p_error function(int, int, int, int, m64p_video_mode, m64p_video_flags) VidExt_SetVideoModeWithRate;

    /* VidExt_ResizeWindow()
    *
    * This function resizes the opengl rendering window to match the given size.
    */
    m64p_error function(int, int) VidExt_ResizeWindow;

    /* VidExt_SetCaption()
    *
    * This function sets the caption text of the emulator rendering window.
    */
    m64p_error function(const char*) VidExt_SetCaption;

    /* VidExt_ToggleFullScreen()
    *
    * This function toggles between fullscreen and windowed rendering modes.
    */
    m64p_error function() VidExt_ToggleFullScreen;

    /* VidExt_GL_GetProcAddress()
    *
    * This function is used to get a pointer to an OpenGL extension function. This
    * is only necessary on the Windows platform, because the OpenGL implementation
    * shipped with Windows only supports OpenGL version 1.1.
    */
    m64p_function function(const char*) VidExt_GL_GetProcAddress;

    /* VidExt_GL_SetAttribute()
    *
    * This function is used to set certain OpenGL attributes which must be
    * specified before creating the rendering window with VidExt_SetVideoMode.
    */
    m64p_error function(m64p_GLattr, int) VidExt_GL_SetAttribute;

    /* VidExt_GL_GetAttribute()
    *
    * This function is used to get the value of OpenGL attributes.  These values may
    * be changed when calling VidExt_SetVideoMode.
    */
    m64p_error function(m64p_GLattr, int*) VidExt_GL_GetAttribute;

    /* VidExt_GL_SwapBuffers()
    *
    * This function is used to swap the front/back buffers after rendering an
    * output video frame.
    */
    m64p_error function() VidExt_GL_SwapBuffers;

    /* VidExt_GL_GetDefaultFramebuffer()
    *
    * On some platforms (for instance, iOS) the default framebuffer object
    * depends on the surface being rendered to, and might be different from 0.
    * This function should be called after VidExt_SetVideoMode to retrieve the
    * name of the default FBO.
    * Calling this function may have performance implications
    * and it should not be called every time the default FBO is bound.
    */
    uint function() VidExt_GL_GetDefaultFramebuffer;
